# 前端

## Project
ant-design-vue + Vue

### 安装
```
cnpm install -g @vue/cli
vue create frontend
cnpm i ant-design-vue --save
```

### 兼容性(未决定添加)
```
cnpm i babel-polyfill --dev
cnpm i babel-plugin-transform-runtime --dev
```

### 高级配置
- 用于按需加载组件代码和样式的 babel 插件
```
yarn add babel-plugin-import --dev
yarn add less@3.0.4 --dev
yarn add less-loader@5.0.0 --dev  "less-loader": "^7.0.2",
```
```
cnpm install vue-cropper --save  // 头像
cnpm install store --save  // 缓存
```

### 定制主题
modifyVars 的方式来进行覆盖变量- 新建vue.config.js文件

### 启动
```
yarn install
yarn dev
```

### 前端token校验
ES6语法（promise、async、await...）

### git冲突
```
git stash
git pull
git stash pop
```

### SVG
```
cnpm install --save-dev vue-svg-icon-loader
cnpm install --save vue-svg-component-runtime
```

#### 删除一行
```javascript
remove (key) {
  const newData = this.data.filter(item => item.key !== key)
  this.data = newData
},
```
#### 查找一行
```javascript
const target = this.data.find(item => item.key === key)
```

const newData = [...this.data] 、  item._originalData = {...item} 
Object.assign()  Object.key(errors).filter(key => errors[key])
