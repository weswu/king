import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/article/page',
    method: 'get',
    params: query
  })
}
export function addObj(data) {
  return request({
    url: '/article/add',
    method: 'post',
    data: data
  })
}
export function delObj(id) {
  return request({
    url: '/article/' + id,
    method: 'delete'
  })
}
export function putObj(data) {
  return request({
    url: '/article/update',
    method: 'put',
    data: data
  })
}
export function getObj(id) {
  return request({
    url: '/article/' + id,
    method: 'get'
  })
}

// 删除图片
export function delImg(data) {
  return request({
    url: '/article/delImg',
    method: 'post',
    data: data
  })
}
