import request from '@/utils/request'

export function getCode(query) {
  return request({
    url: '/code/checkcode',
    method: 'get',
    params: query
  })
}
