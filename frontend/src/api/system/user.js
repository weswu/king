import request from '@/utils/request'

// list
export function fetchList(query) {
  return request({
    url: '/user/page',
    method: 'get',
    params: query
  })
}
