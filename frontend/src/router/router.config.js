// eslint-disable-next-line
import { UserLayout, BasicLayout, BlankLayout } from '@/layouts'

const RouteView = {
  name: 'RouteView',
  render: h => h('router-view')
}

export const asyncRouterMap = [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: 'menu.home' },
    redirect: '/workplace',
    children: [
      // dashboard
      {
        path: '/workplace',
        name: 'workplace',
        component: () => import('@/views/dashboard/Workplace'),
        meta: { title: 'menu.workplace', keepAlive: true, icon: 'dashboard',  permission: ['dashboard'] }
      },
      // list
      {
        path: '/list',
        name: 'list',
        component: RouteView,
        redirect: '/list/table-list',
        meta: { title: 'menu.list', icon: 'table', permission: ['table'] },
        children: [
          {
            path: '/list/basic-list',
            name: 'BasicList',
            component: () => import('@/views/list/BasicList'),
            meta: { title: 'menu.list.basic-list', keepAlive: true, permission: ['table'] }
          },
          {
            path: '/list/card',
            name: 'CardList',
            component: () => import('@/views/list/CardList'),
            meta: { title: 'menu.list.card-list', keepAlive: true, permission: ['table'] }
          },
          {
            path: '/list/search',
            name: 'SearchList',
            component: () => import('@/views/list/search/SearchLayout'),
            redirect: '/list/search/article',
            meta: { title: 'menu.list.search-list', keepAlive: true, permission: ['table'] },
            children: [
              {
                path: '/list/search/article',
                name: 'SearchArticles',
                component: () => import('../views/list/search/Article'),
                meta: { title: 'menu.list.search-list.articles', permission: ['table'] }
              },
              {
                path: '/list/search/project',
                name: 'SearchProjects',
                component: () => import('../views/list/search/Projects'),
                meta: { title: 'menu.list.search-list.projects', permission: ['table'] }
              },
              {
                path: '/list/search/application',
                name: 'SearchApplications',
                component: () => import('../views/list/search/Applications'),
                meta: { title: 'menu.list.search-list.applications', permission: ['table'] }
              }
            ]
          }
        ]
      },
      // profile
      {
        path: '/article', name: 'article',
        component: RouteView,
        redirect: '/article/list',
        meta: { title: '文章管理', icon: 'profile' },
        children: [
          {
            path: '/article/list/:pageNo([1-9]\\d*)?', name: 'articleList',
            component: () => import('@/views/article/articleList'),
            meta: { title: '文章列表', keepAlive: true }
          },
        ]
      },

      // 系统管理
      {
        path: '/system', name: 'system', component: RouteView,
        meta: { title: '系统管理', icon: 'slack' },
        redirect: '/system/icon-selector',
        children: [
          {
            path: '/system/user', name: 'UserList',
            component: () => import('@/views/system/UserList'),
            meta: { title: '用户列表', keepAlive: true }
          },
          {
            path: '/system/role', name: 'RoleList',
            component: () => import('@/views/system/RoleList'),
            meta: { title: '角色列表', keepAlive: true }
          },
          {
            path: '/system/dept', name: 'DeptList',
            component: () => import('@/views/system/DeptList'),
            meta: { title: '部门列表', keepAlive: true }
          },
          {
            path: '/system/permission', name: 'PermissionList',
            component: () => import('@/views/system/PermissionList'),
            meta: { title: '权限列表', keepAlive: true }
          },
          {
            path: '/system/icon-selector', name: 'TestIconSelect',
            component: () => import('@/views/system/IconSelectorView'),
            meta: { title: '图标选择', keepAlive: true }
          },
          {
            path: '/system/list/system-role', name: 'SystemRole',
            component: () => import('@/views/role/RoleList'),
            meta: { title: '角色列表2', keepAlive: true }
          },
        ]
      }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  { path: '/login', name: 'login', component: () => import('@/views/user/login') },
  { path: '/register', name: 'register', component: () => import('@/views/user/register') },
  { path: '/register-result', name: 'registerResult', component: () => import('@/views/user/registerResult') },
  // result
  {
    path: '/result',
    name: 'result',
    component: BasicLayout,
    redirect: '/result/success',
    meta: { title: 'menu.result', icon: 'check-circle-o', permission: ['result'] },
    children: [
      {
        path: '/result/success',
        name: 'ResultSuccess',
        component: () => import('@/views/result/Success'),
        meta: { title: 'menu.result.success', keepAlive: false, hiddenHeaderContent: true, permission: ['result'] }
      },
      {
        path: '/result/fail',
        name: 'ResultFail',
        component: () => import('@/views/result/Error'),
        meta: { title: 'menu.result.fail', keepAlive: false, hiddenHeaderContent: true, permission: ['result'] }
      }
    ]
  },
  // profile
  {
    path: '/profile', name: 'profile', component: BasicLayout,
    redirect: '/profile/basic',
    meta: { title: 'menu.profile', icon: 'profile', permission: ['profile'] },
    children: [
      {
        path: '/profile/basic',
        name: 'ProfileBasic',
        component: () => import('@/views/profile/basic'),
        meta: { title: 'menu.profile.basic', permission: ['profile'] }
      },
      {
        path: '/profile/advanced',
        name: 'ProfileAdvanced',
        component: () => import('@/views/profile/advanced/Advanced'),
        meta: { title: 'menu.profile.advanced', permission: ['profile'] }
      }
    ]
  },
  // account
  {
    path: '/account', name: '用户中心', component: BasicLayout,
    redirect: '/account/center', name: 'account',
    meta: { title: 'menu.account', icon: 'user', keepAlive: true, permission: ['user'] },
    children: [
      {
        path: '/account/center', name: 'center',
        component: () => import('@/views/account/center'),
        meta: { title: 'menu.account.center', keepAlive: true, permission: ['user'] }
      },
      {
        path: '/account/settings', name: 'settings',
        component: () => import('@/views/account/settings/Index'),
        meta: { title: 'menu.account.settings', hideHeader: true, permission: ['user'] },
        redirect: '/account/settings/basic',
        hideChildrenInMenu: true,
        children: [
          {
            path: '/account/settings/basic', name: 'BasicSettings',
            component: () => import('@/views/account/settings/BasicSetting'),
            meta: { title: 'account.settings.menuMap.basic', hidden: true, permission: ['user'] }
          },
          {
            path: '/account/settings/security', name: 'SecuritySettings',
            component: () => import('@/views/account/settings/Security'),
            meta: {
              title: 'account.settings.menuMap.security',
              hidden: true,
              keepAlive: true,
              permission: ['user']
            }
          },
          {
            path: '/account/settings/custom', name: 'CustomSettings',
            component: () => import('@/views/account/settings/Custom'),
            meta: { title: 'account.settings.menuMap.custom', hidden: true, keepAlive: true, permission: ['user'] }
          },
          {
            path: '/account/settings/binding', name: 'BindingSettings',
            component: () => import('@/views/account/settings/Binding'),
            meta: { title: 'account.settings.menuMap.binding', hidden: true, keepAlive: true, permission: ['user'] }
          },
          {
            path: '/account/settings/notification', name: 'NotificationSettings',
            component: () => import('@/views/account/settings/Notification'),
            meta: {
              title: 'account.settings.menuMap.notification',
              hidden: true,
              keepAlive: true,
              permission: ['user']
            }
          }
        ]
      }
    ]
  },
  // Exception
  {
    path: '/exception',
    name: 'exception',
    component: BasicLayout,
    redirect: '/exception/403',
    meta: { title: 'menu.exception', icon: 'warning', permission: ['exception'] },
    children: [
      {
        path: '/exception/403',
        name: 'Exception403',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/403'),
        meta: { title: 'menu.exception.not-permission', permission: ['exception'] }
      },
      {
        path: '/exception/404',
        name: 'Exception404',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404'),
        meta: { title: 'menu.exception.not-find', permission: ['exception'] }
      },
      {
        path: '/exception/500',
        name: 'Exception500',
        component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/500'),
        meta: { title: 'menu.exception.server-error', permission: ['exception'] }
      }
    ]
  },

]
