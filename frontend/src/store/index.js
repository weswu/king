import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
//default router permission control
import permission from './modules/permission'
//import permission from './modules/async-router'
import getters from './getters'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    permission,
    user,
    app
  },
  state: {
    formLayout: {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 13 }
      }
    },
  },
  getters,
  mutations: {
  },
  actions: {
  }
})
