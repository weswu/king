import Mock from 'mockjs2'
import { builder } from '../util'

const user = () => {
  let data = []
  const avatar = ['https://gw.alipayobjects.com/zos/rmsportal/WdGqmHpayyMjiEhcKoVE.png',
    'https://gw.alipayobjects.com/zos/rmsportal/zOsKZmFRdUtvpqCImOVY.png',
    'https://gw.alipayobjects.com/zos/rmsportal/dURIMkkrRFpPgTuzkwnB.png',
    'https://gw.alipayobjects.com/zos/rmsportal/sfjbOqnsXXJgNCjCzDBL.png',
    'https://gw.alipayobjects.com/zos/rmsportal/siCrBXXhmvTQGWPNLBow.png'
  ]
  const name = [
    '付小小',
    '吴加好',
    '周星星',
    '林东东',
    '曲丽丽'
  ]
  const titles = [
    'Alipay',
    'Angular',
    'Ant Design',
    'Ant Design Pro',
    'Bootstrap',
    'React',
    'Vue',
    'Webpack'
  ]

  for (var i = 0; i < 8; i++) {
    const num = parseInt(Math.random() * (4 + 1), 10)
    data.push({
      nickname: name[num],
      username: titles[ i % 8 ],
      avatar: avatar[num],
      deptId: Mock.mock('@integer(1, 999)'),
      roleId: Mock.mock('@integer(1, 999)'),
      createTime: Mock.mock('@datetime'),
    })
  }

  return builder({
    data: data,
    pageSize: 10,
    pageNo: 0,
    totalPage: 1,
    total: 5
  })
}

Mock.mock(/\/user\/page/, 'get', user)
