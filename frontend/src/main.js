import Vue from 'vue'
import router from './router'
import './permission'
import store from './store'
import i18n from './locales'
import App from './App.vue'

import ProLayout, { PageHeaderWrapper } from '@ant-design-vue/pro-layout'
import { STable, Ellipsis } from '@/components'
import themePluginConfig from '../config/themePluginConfig'

import bootstrap from './core/bootstrap'
import './core/lazy_use' // use lazy load 组件
import './utils/filter' // global filter
import './utils/function'
import './styles/index.less'

// mock
// WARNING: `mockjs` NOT SUPPORT `IE` PLEASE DO NOT USE IN `production` ENV.
import './mock'

import Axios from './utils/request'
// ajax
Vue.prototype.$http = Axios

// use pro-layout components
Vue.component('pro-layout', ProLayout)
Vue.component('page-container', PageHeaderWrapper)
Vue.component('page-header-wrapper', PageHeaderWrapper)
Vue.component('s-table', STable)
Vue.component('ellipsis', Ellipsis)
window.umi_plugin_ant_themeVar = themePluginConfig.theme


Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  created: bootstrap,
  render: h => h(App),
}).$mount('#app')
