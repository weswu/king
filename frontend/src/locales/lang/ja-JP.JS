import antd from 'ant-design-vue/es/locale-provider/ja_JP'

const components = {
  antLocale: antd
}

export default {
  ...components
}