import antd from 'ant-design-vue/es/locale-provider/ko_KR'

const components = {
  antLocale: antd
}

export default {
  ...components
}