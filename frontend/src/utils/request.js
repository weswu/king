import axios from 'axios'
import store from '@/store'
import router from '@/router'
import Message from 'ant-design-vue/es/message'
import storage from 'store'
import { ACCESS_TOKEN } from '@/store/mutation-types'

// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 6000 // 请求超时时间
})

// request拦截器，每次发送请求的时候拦截下来
request.interceptors.request.use(config => {
  // 每次发送请求，检查 vuex 中是否有token,如果有放在headers中
  const token = storage.get(ACCESS_TOKEN)
  if (token) {
    config.headers['Authorization'] = token
  }
  return config
}, err => {
  return Promise.reject(err)
})

// 接口返回数据
request.interceptors.response.use(res => {
  const status = res.data.code
  const message = res.data.msg
  if (status && status !== 200) {
    Message.error(message)
    return Promise.reject(new Error(message))
    debugger
  }
  return res.data
}, (error) => {
  // token过期，重新登录
  // 这里为什么处理401错误,详见，server/untils/token check_token这个函数

  if (error.response) {
    if (error.response.status === 401) {
      Message.error('重新登录')
      store.dispatch('Logout').then(() => {
        router.push({ path: '/user/login' })
      })
    }
  }
  return Promise.reject(error)
})

export default request
