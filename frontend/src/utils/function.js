import Vue from 'vue'

let functions = {
  /**
   * 路由跳转
   */
  url (e) {
    this.$router.push({ path: e })
  },
  /**
   * 日期格式化
   */
   dateFormatHandler(date, format) {
     if (!date) return ''
     date = new Date(date)
     format = format || 'yyyy-MM-dd hh:mm'
     var o = {
       'M+': date.getMonth() + 1, // 月份
       'd+': date.getDate(), // 日
       'h+': date.getHours(), // 小时
       'm+': date.getMinutes(), // 分
       's+': date.getSeconds(), // 秒
       'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
       'S': date.getMilliseconds() // 毫秒
     }
     if (/(y+)/.test(format)) {
       format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
     }
     for (var k in o) {
       if (new RegExp('(' + k + ')').test(format)) {
         format = format.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
       }
     }
     return format
  },
  /**
   * 过滤list中的label所有值 多个
   */
  listTostr(val, list, space){
    let text = ''
    val = val || 'label'
    space = space || '、'
    list && list.forEach(item => {
      if(text === '') {
        text = item[val]
      } else {
        text += space + item[val]
      }
    })
    return text
  },
  /**
   * 过滤list中的value  唯一
   */
  valTostr(val, list){
    val = val || ''
    let text = ''
    let color = ''
    list && list.forEach(item => {
      if(item.value.toString() === val.toString()) {
        text = item.label
        color = item.color
      }
    })
    if(color) {
      return `<span style="color: ${color}">${text}</span>`
    } else {
      return text
    }
  },
}


// 加载Vue全局函数
Object.keys(functions).forEach(item => {
  Vue.prototype[item] = functions[item]
})
