import Vue from 'vue'
import moment from 'moment'
import 'moment/locale/zh-cn'
moment.locale('zh-cn')

Vue.filter('NumberFormat', function (value) {
  if (!value) {
    return '0'
  }
  const intPartFormat = value.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') // 将整数部分逢三一断
  return intPartFormat
})

Vue.filter('dayjs', function (dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)
})

Vue.filter('moment', function (dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)
})


/*
 * @author: wes
 * @date: 2017-8-3
 * @desc: 时间 2021-08-03
*/
Vue.filter('date', function (value) {
  if (!value) return ''
  return value.substring(0, 10)
})

/*
 * @author: wes
 * @date: 2017-8-3
 * @desc: 保留字符长度
*/
Vue.filter('limitChar', function (value, start, end) {
  if (!value) return ''
  return value.substring(start, end)
})

/*
 * @author: wes
 * @date: 2017-8-3
 * @desc: 限制字符长度
*/
Vue.filter('limitString', function (value, start, end) {
  if (!value) return ''
  return value.substring(start, value.length - end)
})

/*
 * @author: wes
 * @date: 2017-8-3
 * @desc: 中间过滤 - 手机、邮箱
*/
Vue.filter('limitIdCard', function (str) {
  var reg = /(.{10}).+(.{4})/g
  return str.replace(reg, '$1****$2')
})
