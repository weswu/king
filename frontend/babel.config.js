module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  // lazy load ant-design-vue
  // `style: true` 会加载 less 文件
  plugins: [
    [
      "import",
      { libraryName: "ant-design-vue", libraryDirectory: "es", style: true }
    ]
  ]
}
