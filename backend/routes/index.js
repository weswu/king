const router = require('koa-router')()
const upload = require('../utils/upload')
const controller = require('../controller')
router.get('/', async (ctx, next) => {
  ctx.body = 'Hello world!'
})

router.get('/api/code/checkcode', controller.code.getCode)// 验证码获取
router.post('/api/user/register', controller.user.register)// 注册
router.post('/api/user/login', controller.user.login)// 登陆
router.get('/api/user/info', controller.user.info)
router.get('/api/user/page', controller.user.page)
// 文章
router.get('/api/article/page', controller.article.page)
router.post('/api/article/add', upload.array('img', 5), controller.article.add)// 添加记录
router.delete('/api/article/:id', controller.article.del)// 添加记录
router.put('/api/article/update', upload.array('img', 5), controller.article.update)// 更新记录
router.get('/api/article/:id', controller.article.get)// 获取记录根据id
router.post('/api/article/delImg', controller.article.delImg)// 删除图片

module.exports = router
