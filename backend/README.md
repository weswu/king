# 后端

## Project
koa2 + mongodb + oss

### 安装
```
cnpm install -g koa-generator
```

### 启动
```
yarn install
yarn start
```

### token 生成验证码
```
cnpm i babel-polyfill --dev
cnpm i babel-plugin-transform-runtime --dev
```

### 密码加密
- 用于按需加载组件代码和样式的 babel 插件
```
yarn add babel-plugin-import --dev
yarn add less@3.0.4 --dev
yarn add less-loader@5.0.0 --dev
```

### 定制主题
modifyVars 的方式来进行覆盖变量- 新建vue.config.js文件

### 功能
CURD增删改查
前端校验
上传用户头像
阿里云oss上传图片
token鉴权
模糊搜索
排序
分页
xss内容过滤
请求／响应拦截
...


### 调试
cnpm install -g node-inspector